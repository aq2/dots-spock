" ________  keyboard mapping
  " Quickly edit/reload the vimrc file
  nmap <silent> <leader>ev :e $MYVIMRC<CR>
  nmap <silent> <leader>sv :so $MYVIMRC<CR>

  " lazy man's colon
  nnoremap ; :

  " Tab switches windows and sets pwd
  map <Tab> <C-W>W:cd %:p:h<CR>:<CR>

  " scroll wrapped lines naturally
  nnoremap j gj
  nnoremap k gk

  " quick entry/exit into insert mode
  imap jj <Esc>
  nnoremap <Space> i
  nnoremap <Del> i<Del>

  " Bubble single lines
  nmap <S-Up> ddkP
  imap <S-Up> <ESC>ddkPi
  nmap <S-Down> ddp
  imap <S-Down> <ESC>ddpi
  " Bubble multiple lines
  vmap <S-Up> xkP`[V`]
  vmap <S-Down> xp`[V`]
  imap <S-Up> <ESC>xkP`[V`]i
  imap <S-Down> <ESC>xp`[V`]

  " mark/unmark line  \l marks line, 'l returns to marked line, :match to clear
  nnoremap <silent> <Leader>l ml:execute 'match Search /\%'.line('.').'l/'<CR>
  " \p = permanent marker
  nnoremap <silent> <Leader>p :exe "let m = matchadd('WildMenu','\\%" . line('.') . "l')"<CR>
  " \q clears all, perms and temps
  nnoremap <silent> <Leader>q :call clearmatches()<CR>

  " w!! let's you sudo save a file
  cmap w!! w !sudo tee % >/dev/null

  " remap arrow keys to scroll buffers
  nnoremap <S-Left> :bprev<CR>
  nnoremap <S-Right> :bnext<CR>

  map <Leader>g :Goyo<CR>
  map <Leader>h :nohl<CR>
  map <Leader>m :Limelight!!<CR>
  map <Leader>n :NERDTreeToggle<CR>
  map <silent> <Leader>st :Startify<CR>

  " TODO fucked
  nmap <Leader># <Plug>Commentary

  map q <Nop>



  imap ;; <C-k>->
  imap meh 🤷
  imap :) 🐱
  imap *** *         *           *           *           *
  imap kirk 🔫
  imap spock 🖖
  imap bones ☠️
  imap moto 🛵
  imap uhura 🚀
  imap ^ 
  imap eh? 🤔
  imap kool 😎
  imap omg 😱
  imap phc 📞
  imap turd 💩
  imap vesp 🛵
  imap tux 🐧
  imap dosh 💰


  cmap Q q<CR>
  map <F1> <Esc>
  imap <F1> <Esc>
  cmap waq wqa<CR>
  cmap bro bro o<CR>


  command! Bigger  let &guifont = substitute(&guifont, '\d\+', '\=submatch(0)+1', '')
  command! Smaller let &guifont = substitute(&guifont, '\d\+', '\=submatch(0)-1', '')

  " .... old habits die hard
  inoremap <C-v> <ESC>"+pa
  " vnoremap <C-c> "*y
  vnoremap <C-c> "+y
  vnoremap <C-d> "+d

  nnoremap <Leader>f :FZF<CR>
  nnoremap <Leader>! :FZF!<CR>
  nnoremap <Leader>~ :FZF ~<CR>
  nnoremap <Leader>b :bro o<CR>

  nnoremap <Leader>i :IndentLinesToggle<CR>


"... zoom/restore window.
  function! s:ZoomToggle() abort
    if exists('t:zoomed') && t:zoomed
      execute t:zoom_winrestcmd
      let t:zoomed = 0
    else
      let t:zoom_winrestcmd = winrestcmd()
      resize
      vertical resize
      let t:zoomed = 1
    endif
  endfunction

  command! ZoomToggle call s:ZoomToggle()
  nnoremap <silent> <C-A> :ZoomToggle<CR>

"  ....   syntax stuff
  map <F10> :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
    \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
    \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>


  nmap <leader>sp :call <SID>SynStack()<CR>
  function! <SID>SynStack()
    if !exists("*synstack")
      return
    endif
    echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
  endfunc


" ... bob the nob
