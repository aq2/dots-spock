#!/bin/bash
scrot /tmp/screen.png
convert /tmp/screen.png -scale 10% -scale 1000% /tmp/screen.png

[[ -f $HOME/.config/i3/lock77.png ]] && convert /tmp/screen.png $HOME/.config/i3/lock77.png -gravity center -composite -matte /tmp/screen.png

# dbus-send --print-reply --dest=/org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Stop

i3lock -u -e -i /tmp/screen.png
