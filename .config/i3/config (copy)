# this is my i3 config file
# there are many like it, but this one is mine...


# general settings
set $mod Mod4
new_window pixel 6
font pango: Ubuntu 0
focus_follows_mouse no
floating_modifier $mod
workspace_layout tabbed
hide_edge_borders smart
focus_on_window_activation focus
workspace_auto_back_and_forth yes

# set ~fancy~ silly  workspace names
set $ws1  🗿
set $ws2  🐶
set $ws3  📚

# 🌍 🐧 🐶 🐺 💾 👯 📝 🔥 🛵 📱 🦊 🦇 🐌
#  💩 🤖 😺 🍌 🍒 🏠 🐈 🐒 🐨 🐷 🗿 🏴 ☠️ 

# dark colors - darkest at top
set $blueCharc    #1c1f25
set $rhino        #2f343f
# set $gunpowder    #383c4a
set $bluZodiac    #404552
set $medZodiac    #50586b
set $liteZod      #878ea1

# other colours
set $cobalt       #2c496d
set $tomato       #e39358
set $copperRust   #995555
set $havelockBlue #5294e2

# titlebar colors        border      background  text        indicator
client.focused           $cobalt     $cobalt     $liteZod    $cobalt
client.urgent            $tomato     $tomato     $liteZod
client.unfocused         $blueCharc  $blueCharc  $medZodiac
client.focused_inactive  $blueCharc  $blueCharc  $bluZodiac
# client.background        #ff0000

#----    top bar     ----

# Toggle between bar hide/dock
bindsym $mod+o bar mode toggle

bar {
    mode hide
    position top
    modifier $mod
    tray_output primary
    # font pango:Ubuntu Nerd Font 20
    font pango:Comfortaa Bold 20
    status_command i3blocks -c ~/.config/i3/topbar

    colors {
        background $rhino
        statusline $copperRust
        # ting             border      background  text
        active_workspace   $tomato     $tomato     $medZodiac
        urgent_workspace   $tomato     $tomato     $liteZod
        focused_workspace  $rhino      $bluZodiac  $havelockBlue
        inactive_workspace $rhino      $rhino      $medZodiac
    }
}



#----   startup programs

set $xxn exec --no-startup-id

$xxn megasync &
$xxn xset s 190 190 &
$xxn setxkbmap -option caps:escape
$xxn unclutter --timeout 3 --jitter 5 --fork &
# $xxn rslsync --config ~/.config/resilio.conf
# $xxn /home/angelo/bin/xflux -l 50 -g -3 -k 3400
$xxn feh --no-fehbg --bg-scale ~/Pictures/i3.png &
$xxn xautolock -time 11 -locker '~/.config/i3/lock.sh'
$xxn /usr/lib64/mate-polkit/polkit-mate-authentication-agent-1 &
$xxn compton --backend glx --paint-on-overlay --vsync opengl-swc -f -CGb -D 4 --config /dev/null &


#----    key bindings

bindsym $mod+j workspace prev
bindsym $mod+k workspace next
bindsym $mod+Tab workspace back_and_forth

# kill focused window
bindsym $mod+x kill

# setup rofi launchers
bindsym Mod1+space exec rofi -modi "run#window" -sidebar-mode -show run
bindsym $mod+space exec rofi -lines 6 -width 100 -location 6 -show window

# change focus
bindsym $mod+Up    focus up
bindsym $mod+Left  focus left
bindsym $mod+Down  focus down
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+Up    move up
bindsym $mod+Shift+Left  move left
bindsym $mod+Shift+Down  move down
bindsym $mod+Shift+Right move right

# split horiz/verti
bindsym $mod+h split h
bindsym $mod+v split v

# enter fullscreen mode for the focused container
# bindsym $mod+f fullscreen toggle
bindsym $mod+Page_Up fullscreen toggle

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+Mod2+space focus mode_toggle

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3

# reload/restart config file
bindsym $mod+Shift+c reload
bindsym $mod+Shift+r restart

# resize window (can also use mouse)
bindsym $mod+r mode "resize"
mode "resize" {
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym Up resize shrink height 10 px or 10 ppt
    bindsym Down resize grow height 10 px or 10 ppt
    bindsym Right resize grow width 10 px or 10 ppt
    bindsym Left resize shrink width 10 px or 10 ppt
}

# fancier exitter - better than nagbar!
bindsym $mod+Shift+Escape  exec shutdown now
bindsym $mod+End exec bash ~/.config/i3/shutdown.sh

# change container layout
bindsym $mod+e layout toggle split
bindsym $mod+z layout toggle tabbed split


#----    special apps

# bindsym $mod+t exec 'Tilix' || tilix
bindsym $mod+Return exec mate-terminal
bindsym $mod+a exec wmctrl -x -a 'caja' || caja
bindsym $mod+w exec wmctrl -a 'Firefox' || firefox
bindsym $mod+m $xxn xrdb -merge /home/angelo/.Xresources
bindsym $mod+Print exec scrot -e 'mv $f ~/Pictures/screenshots/'
bindsym $mod+y exec wmctrl -x -a "ranger" || $solomate -e ranger --class="ranger"

#----   special keysz → exec notify-send brightness+
bindsym XF86MonBrightnessUp exec xbacklight -inc 10
bindsym XF86MonBrightnessDown exec xbacklight -dec 5
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5%
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5%


#----    scratchpad

# start a terminal scratchpad
set $mymate exec mate-terminal --disable-factory --class='float'
bindsym $mod+backslash exec wmctrl -x -a 'float' || $mymate; scratchpad show

for_window [class="float"] move scratchpad, move position 10 10
for_window [class="float"] resize set 1100 680, move scratchpad, scratchpad show

# multiple scratchies
bindsym $mod+F1 [con_mark="scr1"] scratchpad show
bindsym $mod+Shift+F1 mark "scr1", move scratchpad

# resize floater only
bindsym $mod+n resize set 800 580, move position 10 10
bindsym $mod+b move position 10 10, resize set 1100 680


#----    floaters

for_window [class="Gpick"] floating enable


# use altgr and fn as other keys
# xmodmap -e 'keycode 108 = asciicircum'



bindsym Print workspace $ws3; exec wmctrl -x -a 'veem'  || mate-terminal --disable-factory --class='veem' -e vim
assign [class='veem'] $ws3
