TERM=xterm-256color
[ -f ~/.config/.fzf.zsh ] && source ~/.config/.fzf.zsh
export PATH=$HOME/bin:/usr/local/bin:$PATH

fpath=("$HOME/.config/zsh" $fpath)
autoload -U colors && colors
autoload -Uz promptinit; promptinit

HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory
HISTFILE=~/.local/share/zsh/zsh_history

prompt pure
PURE_GIT_UP_ARROW=🔺
PURE_GIT_DOWN_ARROW=🔻
PURE_PROMPT_SYMBOL=🗿\ 

PROMPT=" $PROMPT"
setopt prompt_subst
prompt_newline='%666v'
RPROMPT='%{$fg[blue]%}%*'
zle_highlight=(default:bold,fg=white)

function take() {
  mkdir -p $1
  cd $1
}


# key bindings
bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[5~" beginning-of-history
bindkey "\e[6~" end-of-history
bindkey "\e[3~" delete-char
bindkey "\e[2~" quoted-insert
bindkey "\e[5C" forward-word
bindkey "\eOc" emacs-forward-word
bindkey "\e[5D" backward-word
bindkey "\eOd" emacs-backward-word
bindkey "\ee[C" forward-word
bindkey "\ee[D" backward-word
bindkey "^H" backward-delete-word
# for rxvt
bindkey "\e[8~" end-of-line
bindkey "\e[7~" beginning-of-line
# for non RH/Debian xterm, can't hurt for RH/DEbian xterm
bindkey "\eOH" beginning-of-line
bindkey "\eOF" end-of-line
# completion in the middle of a line
bindkey '^i' expand-or-complete-prefix

alias vpn='sudo openvpn ~/Documents/beammeup.ovpn'
alias x='exit'
alias ka='k -A'
alias xx='exit'
alias :q='exit'
alias ..='cd ..'
alias vim=nvim
alias tt='gio trash'
alias free='free -h'
alias gpus='git push' 
alias gpul='git pull'
alias gadd='git add .'
alias gsta='git status'
alias gcom='git commit -m'
alias ls='ls --color=auto'
alias dot="cd ~/.dotfiles"
alias dot2="cd ~/dotfiles"
alias update="sudo apt update"
alias upgrade="sudo apt upgrade"
alias upg="apt list --upgradable"
alias mocp='mocp -T nightly_theme'
alias zeesh='source ~/.dotfiles/zsh/zshrc'
alias zeesh2='source ~/dotfiles/zsh/zshrc'
alias ugrub='sudo grub-mkconfig -o /boot/grub/grub.cfg'
alias glog='git log --pretty="%Cgreen%h %Cblue%ar%Creset %s"'

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'


source /home/angelo/.config/zsh/k.sh
source /home/angelo/.config/zsh/colored-man-pages.plugin
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx

